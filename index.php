<?php
$ler_json = file_get_contents("https://jsonplaceholder.typicode.com/users");   

$decode_user = json_decode($ler_json, true);

if (!empty($decode_user)) {
	usort($decode_user, function ($a, $b) {
	    return $a['name'] <=> $b['name'];
	});
}

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Usuários</title>
	<link rel="stylesheet" href="theme.blue.css">

  </head>
  <body>
    <?php
		echo "<table class='table table-hover tablesorter tablesorter-blue table-condensed' id='tableUsuarios'>";

		echo "<thead>";
			echo "<tr class='tablesorter-headerRow'>";
				echo "<td>Name</td>";
				echo "<td>Username</td>";
				echo "<td>E-mail</td>";
				echo "<td>Telefone</td>";
				echo "<td>Ações</td>";
			echo "</tr>";
		echo "</thead>";

		if (empty($decode_user)) {
			echo "<td colspan='5'>Nenhum dado no momento.</td>";
			die;
		}

		foreach($decode_user as $user){
			echo "<tr>";
				echo "<td>" . $user['name'] . "</td>";
				echo "<td>" . $user['username'] . "</td>";
				echo "<td>" . $user['email'] . "</td>";
				echo "<td>" . $user['phone'] . "</td>";

				echo '<td>';
				echo "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#Modal-".
				$user['id']."'>
  Ver dados 
</button>";
				// gera dados de modal
				modal($user, $user['id']);

				echo '</td>';
			echo "<tr>";
		}

		echo "</table>";

?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script src="jquery.tablesorter.min.js"></script>
    <script src="jquery.tablesorter.widgets.min.js"></script>

    <script type="text/javascript">
		$(document).ready(function(){
			$(function(){
				$("#tableUsuarios").tablesorter(
				{
					theme : 'blue',
					headerTemplate : '{content}{icon}',
					widgetOptions : {
				      columns : [ "primary", "secondary", "tertiary" ]
					}
				});
			});
		});

    </script>
  </body>
</html>


<?php
/*
* $content - dados dos usuarios
* $id - gerado para cada modal apresentado
*/
function modal($content, $id) {
	echo "

	<div class='modal fade bd-example-modal-lg' id='Modal-$id' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
	  <div class='modal-dialog modal-lg' role='document'>
	    <div class='modal-content'>
	      <div class='modal-header'>
	        <h5 class='modal-title' id='exampleModalLabel'>Dados completos usuário</h5>
	        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
	          <span aria-hidden='true'>&times;</span>
	        </button>
	      </div>
	      <div class='modal-body'>
	        <table class='table'>
	        	<tr>
	        		<td>ID</td>
	        		<td>Nome</td>
	        		<td>Endereço</td>
	        		<td>Telefone</td>
	        		<td>Website</td>
	        		<td>Empresa</td>
	        	</tr>
	        	<tr>
	        		<td>{$content['id']}</td>
	        		<td>{$content['name']}</td>
	        		<td>{$content['address']['street']}, {$content['address']['suite']}, {$content['address']['city']} <br />
	        		{$content['address']['zipcode']}<br />
	        		Coordenadas: <br />
	        		Lat:{$content['address']['geo']['lat']} <br/> Long: {$content['address']['geo']['lng']}</td>
	        		<td>{$content['phone']}</td>
	        		<td>{$content['website']}</td>
	        		<td>{$content['company']['name']} <br />{$content['company']['catchPhrase']}<br/> {$content['company']['bs']}</td>
	        	</tr>
	        </table>
	      </div>
	      <div class='modal-footer'>
	        <button type='button' class='btn btn-secondary' data-dismiss='modal'>Fechar</button>
	      </div>
	    </div>
	  </div>
	</div>
";
}
?>